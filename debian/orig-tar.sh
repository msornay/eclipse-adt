#!/bin/bash

#######################################################
# This script fetches the latest commit of the branch #
# with the specified revision.                        #
#######################################################

if [ -d eclipse-adt -o -f eclipse-adt ]
then
    echo "The file/directory 'eclipse-adt' exists ... aborting."
    exit
fi

REVISION=22.6

git clone -b tools_r$REVISION https://android.googlesource.com/platform/sdk eclipse-adt
VERSION=`git --git-dir=eclipse-adt/.git log -1 --format=%cd~%h --date=short | sed s/-//g`
echo "Version "$VERSION

tar -cJf ../eclipse-adt_$REVISION+git$VERSION.orig.tar.xz -C eclipse-adt/ eclipse
rm -rf eclipse-adt